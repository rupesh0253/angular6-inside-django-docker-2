from django.conf.urls import url, include

from home import views
from .views import *

urlpatterns = [
    url(r'^$', views.HomePageView.as_view()),
    url(r'^links/$', views.LinksPageView.as_view()),
    url(r'^registration/',registration),
    url(r'^login/',userlogin),
    url(r'^getdata/',getdata)
    ]