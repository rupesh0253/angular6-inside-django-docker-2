from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.views.generic import TemplateView
from django.shortcuts import render
from .models import  Registration
from django.contrib.auth.models import User
from django.db import transaction
from django.contrib.auth import authenticate,login


class HomePageView(TemplateView):
    def get(self, request, **kwargs):
        return render(request, 'index.html', context=None)


class LinksPageView(TemplateView):
    def get(self, request, **kwargs):
        return render(request, 'links.html', context=None)

@api_view(['POST'])
def registration(request):
    print ("backend",request.data)
    name = request.data ["name"]
    mobileno = request.data ["mobileno"]
    email = request.data ["email"]
    organization = request.data ["organization"]
    password = request.data ["password"]
    with transaction.atomic():
        user = User.objects.create_user(first_name = name, username= mobileno , email=email, password=password)
        user.save()
        p = Registration()
        p.name = name
        p.mobileNo = mobileno
        p.email = email
        p.organazation = organization
        p.password = password
        p.save()
    return Response ("Registration is done")

@api_view(['POST'])
def userlogin(request):
    print ("hi",request.data)
    print ("hello",type (request))
    mobileno = request.data["mobileno"]
    password = request.data["password"]
    user = authenticate(username = mobileno , password = password)
    if user is not None:
        login(request,user)
        return Response("Success")
    else:
        return Response("User not exist")

@api_view(['GET'])
def getdata(request):
    data=login.objects.filter().values()
    return Response(data)

